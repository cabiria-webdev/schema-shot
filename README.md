# Schema Shot
Plugin che inserisce markup JSON-LD *Schema.org* nelle pagine e nei post di WordPress. Aggiunge un filtro per disabilitare i JSON-LD di **Yoast**.

Markup inserito:

* *BlogPosting* (nei post)
* *BreadcrumbList* (nelle pagine / post tramite shortcode)
* *SiteNavigationElement* (in tutte le pagine / post)
* *WebSite* (in homepage)
* *WebPage* (in tutte le pagine / post)
* *LocalBusiness* in tutte le pagine
* *Organization* in una pagina a scelta
* *Menu / MenuSection / MenuItem* (negli appositi *custom post* per i *local business Restaurant*)

Le informazioni che non vengono compilate automaticamente dal plugin sono disponibili nei template *json* della cartella `/assets/templates`.

La visualizzazione dei breadcrumbs usa lo shortcode `[ss_breadcrumbs]`. E' necessario disabilitare i breadcrumbs di **Yoast** per evitare duplicazioni.
